from kivy_deps import sdl2, glew
# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['DiscListing.py'],
             pathex=[],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
a.datas += [('DiscListing.kv','d:\\MSZ\\_Repos\\KiviApp\\DiscListing.kv','DATA')]
a.datas += [('modules\listing.py','d:\\MSZ\\_Repos\\KiviApp\\modules\\listing.py','DATA')]
a.datas += [('ico\\listing_ico.ico','d:\\MSZ\\_Repos\\KiviApp\\ico\\listing_ico.ico','DATA')]
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
		  *[Tree(p) for p in (sdl2.dep_bins)],		  
          [],
          name='DiscListing',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
		  icon='d:\\MSZ\\_Repos\\KiviApp\\ico\\listing_ico.ico',
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
