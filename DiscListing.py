from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.app import App
from kivy.core.window import Window
import tkinter as tk
from tkinter import filedialog
import os
from modules.listing import Listing
Window.size = (700, 350)

class Main(BoxLayout):
    # variable to store path dir
    pathVar=''

    def getInputPath(self):
        self.pathVar = ''
        root = tk.Tk()
        root.withdraw()
        pathVar = filedialog.askdirectory()
        self.ids.InputPath.text = pathVar  # Change text of InputPath
        root.destroy()


    def getDiscName(self):
        return self.ids.DiscNameInput.text


    #start button function    
    def start(self):
        Listing(self.ids.InputPath.text, self.getDiscName())
        self.message('[b]Work finished[/b]')        
   

    #message box - show message
    def message(self, message):
        self.ids.Message.text = message


class DiscListing(App):
    def build(self):
        self.icon = "d:\MSZ\_Repos\KiviApp\ico\listing_ico.ico"
        return Main()
 

if __name__ == "__main__":
    DiscListing().run()