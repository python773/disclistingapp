# -*- coding: utf-8 -*-
import os
import datetime
from collections import Counter


class Listing():

    def __init__(self, path, disc_name=''):
        self.path = path
        self.disc_name = disc_name
        self.file_list = [] #files list
        self.file_extensions = [] #extensions list
        self.unique_extensions = [] #unique extensions list 
        self.message=''
        self.getFiles()
        self.save_listing_file()
        self.save_statistic_file()


    def getFiles(self):  
        try:
            for root, dirs, files in os.walk(self.path):
                for name in files:
                    add=os.path.join(root,name)
                    if not "$RECYCLE" in str(add):
                        self.file_list.append(add) #file list
                        filename, file_extension = os.path.splitext(name)
                        self.file_extensions.append(file_extension)     
            
            self.file_number=len(self.file_list) #count files go listing
            self.unique_extensions=list(set(self.file_extensions)) #get unique values from extensions list
        except:
            return "Error while getting file list."
        return "File list prepared correctly"

    def save_listing_file(self):
        with open(os.path.join(self.path,f"{self.disc_name}-listing.txt"),"w",encoding='utf-8') as f: 
           f.write("File with listing from folder: " + self.path + "\n" +"\n"+ "name ; path ; size [B] ; creation_time ; modification_time ; last_use_time\n")  
           i=0
           for file in self.file_list:
                try:
                    file.encode('utf-8')
                    f.write("\n {} ; {} ; {} ; {} ; {} ; {}".format(str(file[file.rfind("\\",)+1:]), file, str(os.path.getsize(file)), str(datetime.datetime.fromtimestamp(os.path.getctime(file))),\
                    str(datetime.datetime.fromtimestamp(os.path.getmtime(file))), str(datetime.datetime.fromtimestamp(os.path.getatime(file)))))
                    i+=1
                except:
                    return f"Problem with saving listing on file {file}"
        return "Listing file saved correctly"
    

    def save_statistic_file(self):  #create statistic file            
        key=list(Counter(self.file_extensions).keys())
        value=list(Counter(self.file_extensions).values())
        
        with open(os.path.join(self.path,f"{self.disc_name}-statistics.txt"),"w") as f:
            try:
                f.write("Statistics: " + self.path + "\n" +"\n")
                f.write("All files: " + str(len(self.file_list))+"\n")
                for i in range (0,len(key)):
                    f.write(" *"+str(key[i]) + " - " + str(value[i])+"\n")=="*"
            except:
                return "Problem with saving statistic file"
        return "Statistics file saved correctly"



